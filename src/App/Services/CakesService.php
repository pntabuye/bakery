<?php

namespace App\Services;

class CakesService extends BaseService
{
    // Not actually ALL but random 10 first
    public function getAll()
    {
        return $this->db->fetchAll("SELECT * FROM cakes ORDER BY RAND() LIMIT 10");
    }
    // Same as above by with a fixed type
    public function getAllByType($cakeType)
    {
        return $this->db->fetchAll("SELECT * FROM cakes WHERE cakeType=? ORDER BY RAND() LIMIT 10", [$cakeType]);
    }
    // NOTE: Recipie existence validator
    public function getOneByRecipe($cake)
    {
        $extraSql = "";
        if(array_key_exists('topping_flavour', $cake) && !empty($cake['topping_flavour']))
          $extraSql .= " AND topping_flavour='".$cake['topping_flavour']."'";

        if(array_key_exists('filling_flavour', $cake) && !empty($cake['filling_flavour']))
          $extraSql .= " AND filling_flavour='".$cake['filling_flavour']."'";

        return $this->db->fetchall("SELECT * from cakes WHERE cakeType=? AND pastry=? AND topping=? AND filling=? $extraSql LIMIT 1",
        [$cake['cakeType'], $cake['pastry'], $cake['topping'], $cake['filling']]);
    }

    //function save($cake)
    //{
    //    $this->db->insert("cakes", $cake);
    //    return $this->db->lastInsertId();
    //}
    //
    // function update($id, $cake)
    // {
    //     return $this->db->update('cakes', $cake, ['id' => $id]);
    // }
    //
    // function delete($id)
    // {
    //     return $this->db->delete("cakes", array("id" => $id));
    // }

}
