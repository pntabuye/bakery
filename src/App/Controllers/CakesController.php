<?php

namespace App\Controllers;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CakesController
{

    protected $cakesService;

    public function __construct($service)
    {
        $this->cakesService = $service;
    }

    // public function getOne($id)
    // {
    //     return new JsonResponse($this->cakesService->getOne($id));
    // }

    public function getAll()
    {
        return new JsonResponse($this->cakesService->getAll());
    }

    public function getAllByType($cakeType)
    {
        return new JsonResponse($this->cakesService->getAllByType($cakeType));
    }

    public function order(Request $request)
    {
        $cake = $this->getDataFromRequest($request);
        $res = $this->cakesService->getOneByRecipe($cake);
        if(empty($res))
        {
            return new JsonResponse(array("message" => "Recipe is not available"));
        } else {
            $res[0]["message"] = "Order accepted";
            return new JsonResponse($res[0]);
        }
    }

    // public function save(Request $request)
    // {
    //     $cake = $this->getDataFromRequest($request);
    //     return new JsonResponse(array("id" => $this->cakesService->save($cakeType)));
    // }
    //
    // public function update($id, Request $request)
    // {
    //     $cake = $this->getDataFromRequest($request);
    //     $this->cakesService->update($id, $cakeType);
    //     return new JsonResponse($cakeType);
    //
    // }
    //
    // public function delete($id)
    // {
    //
    //     return new JsonResponse($this->cakesService->delete($id));
    //
    // }

    public function getDataFromRequest(Request $request)
    {
        return $cake = array(
            "cakeType" => $request->request->get("cakeType"),
            "pastry" => $request->request->get("pastry"),
            "topping" => $request->request->get("topping"),
            "topping_flavour" => $request->request->get("topping_flavour"),
            "filling" => $request->request->get("filling"),
            "filling_flavour" => $request->request->get("filling_flavour")
        );
    }
}
