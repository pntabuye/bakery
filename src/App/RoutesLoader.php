<?php

namespace App;

use Silex\Application;

class RoutesLoader
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->instantiateControllers();

    }

    private function instantiateControllers()
    {
        $this->app['cakes.controller'] = function() {
            return new Controllers\CakesController($this->app['cakes.service']);
        };
    }

    public function bindRoutesToControllers()
    {
        $api = $this->app["controllers_factory"];
        // Supported routes
        $api->get('/cakes', "cakes.controller:getAll");
        $api->get('/cakes/{cakeType}', "cakes.controller:getAllByType");
        $api->post('/cakes/order', "cakes.controller:order");
        //$api->get('/cakes/{id}', "cakes.controller:getOne");
        //$api->post('/cakes', "cakes.controller:save");
        //$api->put('/cakes/{id}', "cakes.controller:update");
        //$api->delete('/cakes/{id}', "cakes.controller:delete");

        $this->app->mount($this->app["api.endpoint"].'/'.$this->app["api.version"], $api);
    }
}
