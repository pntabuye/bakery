# BAKERY : simple PHP REST API using Silex

A simple RESTFull API built with Silex.
Fork of the work one by [Alessandro Arnodo](http://alessandro.arnodo.net).

#### Making it run:

From the root folder run the following commands

	composer install
	mysql --user=root < resources/sql/schema.sql
	php -S 0:9001 -t web/

### Running tests:

From the root folder run the following command to run tests.

vendor/bin/phpunit

#### What you get:

The api will respond to

	GET  ->   http://localhost:9001/api/v1/cakes
	GET  ->   http://localhost:9001/api/v1/cakes/{cakeType}
	POST ->   http://localhost:9001/api/v1/cakes/order
