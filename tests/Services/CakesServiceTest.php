<?php

namespace Tests\Services;
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use App\Services\CakesService;


class CakesServiceTest extends \PHPUnit_Framework_TestCase
{

    private $cakeService;

    public function setUp()
    {
        $app = new Application();
        $app->register(new DoctrineServiceProvider(), array(
            "db.options" => array(
              "driver" => "pdo_mysql",
              "user" => "root",
              "password" => "",
              "dbname" => "cakesdb",
              "host" => "127.0.0.1",
              "port" => 3306,
            ),
        ));

        $this->cakeService = new CakesService($app["db"]);

        $stmt = $app["db"]->prepare("CREATE TABLE IF NOT EXISTS flavours (
            id       INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
            flavour  VARCHAR(255) NOT NULL,
            UNIQUE (flavour)
        )");
        $stmt->execute();

        $stmt = $app["db"]->prepare("CREATE TABLE IF NOT EXISTS creams (
            id       INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
            cream  VARCHAR(255) NOT NULL,
            UNIQUE (cream)
        )");
        $stmt->execute();

        $stmt = $app["db"]->prepare("CREATE TABLE IF NOT EXISTS recipes (
            id      INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
            cakeName VARCHAR(255) NOT NULL,
            pastry VARCHAR(255) NOT NULL,
            topping VARCHAR(255) DEFAULT NULL,
            filling VARCHAR(255) DEFAULT NULL,
            UNIQUE (cakeName, pastry, topping, filling)
        )");
        $stmt->execute();

        $stmt = $app["db"]->prepare("CREATE OR REPLACE VIEW cakes AS
        SELECT r.cakeName as cakeType,
        r.pastry,
        r.topping,
        c1.cream as topping_cream,
        f1.flavour as topping_flavour,
        r.filling,
        c2.cream as filling_cream,
        f2.flavour as filling_flavour
        FROM recipes r
        LEFT OUTER JOIN creams c1 ON r.topping=c1.cream
        LEFT OUTER JOIN flavours f1 ON c1.cream IS NOT NULL
        LEFT OUTER JOIN creams c2 ON r.filling=c2.cream
        LEFT JOIN flavours f2 ON c2.cream IS NOT NULL
        ORDER BY r.cakeName, r.pastry, r.topping, r.filling");
        $stmt->execute();
    }

    // public function testGetOne()
    // {
    //     $data = $this->cakeService->getOne(1);
    //     $this->assertEquals('dummycake', $data['cake']);
    // }

    public function testGetAll()
    {
        $data = $this->cakeService->getAll();
        $this->assertNotNull($data);
    }

    public function testGetAllByType()
    {
        $data = $this->cakeService->getAll("Eclairs");
        $this->assertNotNull($data);
    }

    public function testGetOneByRecipe()
    {
        $cakeMock = [
          "cakeType" => "Eclairs",
          "pastry" => "Choux pastry",
          "topping" => "Chocolate",
          "topping_cream" => null,
          "topping_flavour" => null,
          "filling" => "Whipped crème",
          "filling_cream" => "Whipped crème",
          "filling_flavour" => "Vanilla",
          "message" => "Order accepted"
        ];
        $data = $this->cakeService->getOneByRecipe($cakeMock);
        $this->assertNotNull($data);
    }

    public function testGetOneByRecipeUnavailable()
    {
        $cakeMock = [
          "cakeType" => "Thunder",
          "pastry" => "Choux pastry",
          "topping" => "Chocolate",
          "topping_cream" => null,
          "topping_flavour" => null,
          "filling" => "Whipped crème",
          "filling_cream" => "Whipped crème",
          "filling_flavour" => "Vanilla",
          "message" => "Order accepted"
        ];
        $data = $this->cakeService->getOneByRecipe($cakeMock);
        $this->assertEmpty($data);
    }
    // function testSave()
    // {
    //     $cake = array("cake" => "arny");
    //     $data = $this->cakeService->save($cake);
    //     $data = $this->cakeService->getAll();
    //     $this->assertEquals(2, count($data));
    // }

    // function testUpdate()
    // {
    //     $cake = array("cake" => "arny1");
    //     $this->cakeService->save($cake);
    //     $cake = array("cake" => "arny2");
    //     $this->cakeService->update(1, $cake);
    //     $data = $this->cakeService->getAll();
    //     $this->assertEquals("arny2", $data[0]["cake"]);
    //
    // }
    //
    // function testDelete()
    // {
    //     $cake = array("cake" => "arny1");
    //     $this->cakeService->save($cake);
    //     $this->cakeService->delete(1);
    //     $data = $this->cakeService->getAll();
    //     $this->assertEquals(1, count($data));
    // }
}
