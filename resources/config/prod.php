<?php
$app['log.level'] = Monolog\Logger::ERROR;
$app['api.version'] = "v1";
$app['api.endpoint'] = "/api";

/**
 * SQLite database file
 */
//$app['db.options'] = array(
//    'driver' => 'pdo_sqlite',
//    'path' => realpath(ROOT_PATH . '/app.db'),
//);

/**
 * MySQL
 */
$app['db.options'] = array(
  "driver" => "pdo_mysql",
  "user" => "root",
  "password" => "",
  "dbname" => "cakesdb",
  "host" => "127.0.0.1",
  "port" => 3306,
);
