<?php
require __DIR__ . '/prod.php';
$app['debug'] = true;
$app['log.level'] = Monolog\Logger::DEBUG;

/**
* SQLite
*/
//$app['db.options'] = array(
//  'driver' => 'pdo_sqlite',
//  'path' => realpath(ROOT_PATH . '/app.db'),
//);

/**
 * MySQL
 */
$app['db.options'] = array(
  "driver" => "pdo_mysql",
  "user" => "root",
  "password" => "",
  "dbname" => "cakesdb",
  "host" => "127.0.0.1",
  "port" => 3306,
);
