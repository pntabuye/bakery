CREATE DATABASE IF NOT EXISTS cakesdb;

USE cakesdb;

CREATE TABLE IF NOT EXISTS flavours (
    id       INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    flavour  VARCHAR(255) NOT NULL,
    UNIQUE (flavour)
) DEFAULT CHARSET=utf8;

insert into flavours (flavour) value ( 'Plain' );
insert into flavours (flavour) value ( 'Coffee' );
insert into flavours (flavour) value ( 'Orange' );
insert into flavours (flavour) value ( 'Lemon' );
insert into flavours (flavour) value ( 'Vanilla' );

CREATE TABLE IF NOT EXISTS creams (
    id       INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cream  VARCHAR(255) NOT NULL,
    UNIQUE (cream)
) DEFAULT CHARSET=utf8;

insert into creams (cream) value ('Butter creme');
insert into creams (cream) value ('Whipped creme');
insert into creams (cream) value ('Cheesecake creme (secret recipe)');

CREATE TABLE IF NOT EXISTS recipes (
    id      INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cakeName VARCHAR(255) NOT NULL,
    pastry VARCHAR(255) NOT NULL,
    topping VARCHAR(255) DEFAULT NULL,
    filling VARCHAR(255) DEFAULT NULL,
    UNIQUE (cakeName, pastry, topping, filling)
) DEFAULT CHARSET=utf8;

/* Cupcakes */
insert into recipes (cakeName,pastry,topping,filling)
values ('Cupcakes','Sponge', 'Icing', NULL);
insert into recipes (cakeName,pastry,topping,filling)
values ('Cupcakes','Sponge', 'Butter creme', NULL);
insert into recipes (cakeName,pastry,topping,filling)
values ('Cupcakes','Sponge', 'Whipped creme', NULL);
/* Eclaires */
insert into recipes (cakeName,pastry,topping,filling)
values ('Eclairs','Choux pastry','Chocolate', 'Butter creme');
insert into recipes (cakeName,pastry,topping,filling)
values ('Eclairs','Choux pastry','Chocolate', 'Whipped creme');
/* Mille-feuilles */
insert into recipes (cakeName,pastry,topping,filling)
values ('Mille-feuilles','Puff pastry','Fruits', 'Butter creme');
insert into recipes (cakeName,pastry,topping,filling)
values ('Mille-feuilles','Puff pastry','Fruits', 'Whipped creme');
/* Cheesecake */
insert into recipes (cakeName,pastry,topping,filling)
values ('Cheesecake','Shortcrust', NULL,'Cheesecake creme (secret recipe)');

/* All cakes available with their flavours */

CREATE OR REPLACE VIEW cakes AS
SELECT r.cakeName as cakeType,
r.pastry,
r.topping,
c1.cream as topping_cream,
f1.flavour as topping_flavour,
r.filling,
c2.cream as filling_cream,
f2.flavour as filling_flavour
FROM recipes r
LEFT OUTER JOIN creams c1 ON r.topping=c1.cream
LEFT OUTER JOIN flavours f1 ON c1.cream IS NOT NULL
LEFT OUTER JOIN creams c2 ON r.filling=c2.cream
LEFT JOIN flavours f2 ON c2.cream IS NOT NULL
ORDER BY r.cakeName, r.pastry, r.topping, r.filling;

/* A table to hold orders */
CREATE TABLE IF NOT EXISTS recipes (
    id      INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cakeName VARCHAR(255),
    pastry VARCHAR(255),
    topping VARCHAR(255),
    filling VARCHAR(255),
    flavour VARCHAR(255)
) DEFAULT CHARSET=utf8;
